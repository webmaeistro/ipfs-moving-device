package threads.lite.dag;

import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Objects;
import java.util.Stack;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import unixfs.pb.Unixfs;

public class DagWalker {

    private final DagService dagService;
    private final Merkledag.PBNode root;

    private DagWalker(@NonNull Merkledag.PBNode node, @NonNull DagService dagService) {
        this.root = node;
        this.dagService = dagService;
    }

    public static DagWalker createWalker(@NonNull Merkledag.PBNode node, @NonNull DagService dagService) {
        return new DagWalker(node, dagService);
    }


    @Nullable
    public DagStage next(@NonNull Cancellable cancellable, @NonNull DagVisitor dagVisitor)
            throws Exception {


        if (!dagVisitor.isRootVisited(true)) {
            DagStage dagStage = dagVisitor.peekStage();
            Objects.requireNonNull(dagStage);
            if (dagStage.getNode().equals(root)) {
                return dagStage;
            }
        }

        if (dagVisitor.isPresent()) {
            boolean success = down(cancellable, dagVisitor);
            if (success) {
                DagStage dagStage = dagVisitor.peekStage();
                Objects.requireNonNull(dagStage);
                return dagStage;
            }

            success = up(dagVisitor);

            if (success) {
                return next(cancellable, dagVisitor);
            }
        }
        return null; // done, nothing left
    }

    private boolean up(@NonNull DagVisitor dagVisitor) {

        if (dagVisitor.isPresent()) {
            dagVisitor.popStage();
        } else {
            return false;
        }
        if (dagVisitor.isPresent()) {
            boolean result = nextChild(dagVisitor);
            if (result) {
                return true;
            } else {
                return up(dagVisitor);
            }
        } else {
            return false;
        }
    }


    private boolean nextChild(@NonNull DagVisitor dagVisitor) {
        DagStage dagStage = dagVisitor.peekStage();
        Merkledag.PBNode activeNode = dagStage.getNode();

        if (dagStage.index() + 1 < activeNode.getLinksCount()) {
            dagStage.incrementIndex();
            return true;
        }

        return false;
    }


    public boolean down(@NonNull Cancellable cancellable, @NonNull DagVisitor dagVisitor)
            throws Exception {

        Merkledag.PBNode child = fetchChild(cancellable, dagVisitor);
        if (child != null) {
            dagVisitor.pushActiveNode(child);
            return true;
        }
        return false;
    }


    @Nullable
    private Merkledag.PBNode fetchChild(@NonNull Cancellable cancellable, @NonNull DagVisitor dagVisitor)
            throws Exception {
        DagStage dagStage = dagVisitor.peekStage();
        Merkledag.PBNode activeNode = dagStage.getNode();
        int index = dagStage.index();
        Objects.requireNonNull(activeNode);

        if (index >= activeNode.getLinksCount()) {
            return null;
        }

        return dagService.getNode(cancellable, getChild(activeNode, index));
    }

    @NonNull
    public Merkledag.PBNode getRoot() {
        return root;
    }

    public Pair<Stack<DagStage>, Long> seek(@NonNull Cancellable cancellable,
                                            @NonNull Stack<DagStage> stack,
                                            long offset) throws Exception {

        if (offset < 0) {
            throw new Exception("invalid offset");
        }

        if (offset == 0) {
            return Pair.create(stack, 0L);
        }

        long left = offset;
        DagStage peek = stack.peek();
        Merkledag.PBNode node = peek.getNode();

        if (node.getLinksCount() > 0) {
            // Internal node, should be a `mdag.ProtoNode` containing a
            // `unixfs.FSNode` (see the `balanced` package for more details).
            Unixfs.Data unixData = peek.getData();

            // If there aren't enough size hints don't seek
            // (see the `io.EOF` handling error comment below).
            if (unixData.getBlocksizesCount() != node.getLinksCount()) {
                throw new Exception("ErrSeekNotSupported");
            }


            // Internal nodes have no data, so just iterate through the
            // sizes of its children (advancing the child index of the
            // `dagWalker`) to find where we need to go down to next in
            // the search
            for (int i = 0; i < unixData.getBlocksizesCount(); i++) {

                long childSize = unixData.getBlocksizes(i);

                if (childSize > left) {
                    stack.peek().setIndex(i);

                    Merkledag.PBNode fetched = dagService.getNode(cancellable, getChild(node, i));
                    stack.push(new DagStage(fetched));

                    return seek(cancellable, stack, left);
                }
                left -= childSize;
            }
        }

        return Pair.create(stack, left);
    }

    @NonNull
    public Cid getChild(@NonNull Merkledag.PBNode node, int index) {
        return Cid.decode(node.getLinks(index).getHash().toByteArray());
    }

    public Pair<Stack<DagStage>, Long> seek(@NonNull Cancellable cancellable, long offset)
            throws Exception {

        Stack<DagStage> stack = new Stack<>();
        stack.push(new DagStage(getRoot()));

        return seek(cancellable, stack, offset);

    }
}

