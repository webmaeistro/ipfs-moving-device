package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import threads.lite.cid.Block;
import threads.lite.cid.Cid;

public interface BlockStore {

    boolean hasBlock(@NonNull Cid cid);

    @Nullable
    Block getBlock(@NonNull Cid cid);

    void deleteBlocks(@NonNull List<Cid> cids);

    void storeBlock(@NonNull Block block);

    void clear();
}


