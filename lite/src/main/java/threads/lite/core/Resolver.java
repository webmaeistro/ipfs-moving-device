package threads.lite.core;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.protobuf.ByteString;

import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.IPV;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.Multiaddrs;
import threads.lite.cid.PeerId;
import threads.lite.minidns.DnsClient;
import threads.lite.minidns.DnsResolver;

public class Resolver {

    private static final String TAG = Resolver.class.getSimpleName();

    private final Supplier<IPV> ipv;
    private final DnsClient dnsClient;

    public Resolver(DnsClient dnsClient, Supplier<IPV> ipv) {
        this.dnsClient = dnsClient;
        this.ipv = ipv;
    }

    @Nullable
    public Multiaddr resolveToOne(Multiaddr multiaddr) {
        Multiaddrs resolvedAddrs = new Multiaddrs();

        resolvedAddrs.addAll(resolveAddress(multiaddr));

        return resolved(resolvedAddrs);
    }

    // this includes circuit addresses
    public Multiaddrs resolve(PeerId peerId, List<ByteString> byteStrings) {
        Multiaddrs multiaddrs = Multiaddr.create(peerId, byteStrings);
        Multiaddrs resolvedAddrs = new Multiaddrs();
        for (Multiaddr multiaddr : multiaddrs) {
            resolvedAddrs.addAll(resolveAddress(multiaddr));
        }
        Multiaddrs result = new Multiaddrs();
        for (Multiaddr multiaddr : resolvedAddrs) {
            if (multiaddr.protocolSupported(ipv().get())) {
                if (isReachable(multiaddr)) { // only reachable
                    result.add(multiaddr);
                }
            }
        }
        return result;
    }

    // this ignores circuit addresses
    @Nullable
    public Multiaddr resolveToOne(PeerId peerId, List<ByteString> byteStrings) {
        Multiaddrs multiaddrs = Multiaddr.create(peerId, byteStrings, ipv().get());
        return resolveToOne(multiaddrs);
    }


    @Nullable
    public Multiaddr resolveToOne(Multiaddrs multiaddrs) {
        if (multiaddrs.isEmpty()) {
            return null;
        }
        Multiaddrs resolvedAddrs = new Multiaddrs();
        for (Multiaddr multiaddr : multiaddrs) {
            resolvedAddrs.addAll(resolveAddress(multiaddr));
        }
        return resolved(resolvedAddrs);
    }

    @Nullable
    private Multiaddr resolved(Multiaddrs resolvedAddrs) {
        if (resolvedAddrs.isEmpty()) {
            return null;
        }
        // the first one will not be checked if it is reachable [hole punching]
        if (resolvedAddrs.size() == 1) {
            return resolvedAddrs.iterator().next();
        }

        for (Multiaddr resolvedAddr : resolvedAddrs) {
            if (isReachable(resolvedAddr)) { // only reachable
                return resolvedAddr;
            }
        }
        return null;
    }


    private boolean isReachable(Multiaddr multiaddr) {
        try {
            return multiaddr.getInetAddress().isReachable(IPFS.RESOLVE_TIMEOUT);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, multiaddr.toString() + " " + throwable.getMessage());
        }
        return false;
    }

    @NonNull
    private Set<Multiaddr> resolveAddress(@NonNull Multiaddr multiaddr) {
        if (multiaddr.isDns() || multiaddr.isDns6() || multiaddr.isDns4()) {
            Multiaddr resolved = DnsResolver.resolveDns(dnsClient, multiaddr);
            if (resolved != null) {
                return Set.of(resolved);
            }
        } else if (multiaddr.isDnsaddr()) {
            return DnsResolver.resolveDnsaddr(dnsClient, ipv.get(), multiaddr);
        } else {
            return Set.of(multiaddr);
        }
        return Set.of();
    }

    public Supplier<IPV> ipv() {
        return ipv;
    }
}
