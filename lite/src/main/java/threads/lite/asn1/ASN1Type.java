package threads.lite.asn1;

abstract class ASN1Type {
    final Class<? extends ASN1Object> javaClass;

    ASN1Type(Class<? extends ASN1Object> javaClass) {
        this.javaClass = javaClass;
    }

    public final boolean equals(Object that) {
        return this == that;
    }

}
