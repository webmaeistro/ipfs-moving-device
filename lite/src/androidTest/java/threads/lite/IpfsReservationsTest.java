package threads.lite;


import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import threads.lite.cid.Multiaddr;
import threads.lite.cid.Network;
import threads.lite.core.Connection;
import threads.lite.core.NatType;
import threads.lite.core.Reservation;
import threads.lite.core.Server;
import threads.lite.core.Session;


@RunWith(AndroidJUnit4.class)
public class IpfsReservationsTest {
    private static final String TAG = IpfsReservationsTest.class.getSimpleName();
    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }

    @Test
    public void test_reservations() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);
        Server server = TestEnv.getServer();
        assertNotNull(server);

        if (!Network.isNetworkConnected(context)) {
            LogUtils.warning(TAG, "nothing to test here NO NETWORK");
            return;
        }

        if (server.getNatType() != NatType.RESTRICTED_CONE) {
            LogUtils.warning(TAG, "nothing to test here NAT TYPE is not RESTRICTED_CONE");
            return;
        }

        assertTrue(ipfs.hasReservations(server));

        try (Session ignored = ipfs.createSession()) {

            assertNotNull(ignored.getPeerStore());

            int timeInMinutes = 1; // make higher for long run


            for (Multiaddr ma : ipfs.getIdentity(server).getMultiaddrs()) {
                LogUtils.debug(TAG, ma.toString());
            }

            // test 1 minutes
            for (int i = 0; i < timeInMinutes; i++) {
                Thread.sleep(TimeUnit.MINUTES.toMillis(1));

                Set<Reservation> reservations = ipfs.reservations(server);
                for (Reservation reservation : reservations) {
                    LogUtils.debug(TAG, "Expire in minutes " + reservation.expireInMinutes()
                            + " " + reservation);
                    Connection conn = reservation.getConnection();
                    assertNotNull(conn);
                    assertTrue(conn.isConnected());
                    assertNotNull(conn.getRemoteAddress());
                }
            }
        }
    }

}
