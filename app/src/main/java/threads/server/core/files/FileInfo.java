package threads.server.core.files;

import android.provider.DocumentsContract;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import java.util.Objects;
import java.util.UUID;

import threads.lite.cid.Cid;

@androidx.room.Entity
public class FileInfo {

    @ColumnInfo(name = "parent")
    private final long parent; // checked
    @NonNull
    @ColumnInfo(name = "name")
    private final String name; // checked
    @PrimaryKey(autoGenerate = true)
    private long idx; // checked
    @ColumnInfo(name = "lastModified")
    private long lastModified; // checked
    @Nullable
    @ColumnInfo(name = "cid")
    @TypeConverters(FileInfo.class)
    private Cid cid; // checked
    @ColumnInfo(name = "size")
    private long size;  // checked
    @NonNull
    @ColumnInfo(name = "mimeType")
    private String mimeType;  // checked
    @Nullable
    @ColumnInfo(name = "uri")
    private String uri; // checked
    @Nullable
    @ColumnInfo(name = "work")
    private String work; // checked
    @ColumnInfo(name = "deleting")
    private boolean deleting; // checked

    FileInfo(@NonNull String name, long parent) {
        this.name = name;
        this.parent = parent;
        this.cid = null;
        this.lastModified = System.currentTimeMillis();
        this.mimeType = "";
        this.deleting = false;
        this.work = null;
    }


    @Nullable
    @TypeConverter
    public static Cid fromArray(byte[] data) {
        return Cid.fromArray(data);
    }

    @Nullable
    @TypeConverter
    public static byte[] toArray(Cid cid) {
        return Cid.toArray(cid);
    }

    static FileInfo createFileInfo(@NonNull String name, long parent) {
        return new FileInfo(name, parent);
    }

    @Nullable
    public Cid getCid() {
        return cid;
    }

    public void setCid(@Nullable Cid cid) {
        this.cid = cid;
    }

    @Nullable
    public String getUri() {
        return uri;
    }

    public void setUri(@Nullable String uri) {
        this.uri = uri;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public long getIdx() {
        return idx;
    }

    void setIdx(long idx) {
        this.idx = idx;
    }

    @NonNull
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(@NonNull String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileInfo fileInfo = (FileInfo) o;
        return getIdx() == fileInfo.getIdx();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdx());
    }

    public long getParent() {
        return parent;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public boolean isDir() {
        return DocumentsContract.Document.MIME_TYPE_DIR.equals(getMimeType());
    }

    public boolean isDeleting() {
        return deleting;
    }

    public void setDeleting(boolean deleting) {
        this.deleting = deleting;
    }

    @Nullable
    public String getWork() {
        return work;
    }

    public void setWork(@Nullable String work) {
        this.work = work;
    }

    @Nullable
    public UUID getWorkUUID() {
        if (work != null) {
            return UUID.fromString(work);
        }
        return null;
    }


}
